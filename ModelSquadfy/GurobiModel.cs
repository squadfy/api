﻿using System;
using Gurobi;

namespace ModelSquadfy
{
    public class GurobiModel
    {
        //Settando variáveis globais
        static GRBEnv env = new GRBEnv("test.log");
        static GRBModel model = new GRBModel(env);

        private Double valorZ = model.ObjVal;
        private Double E = 0;

        static readonly String[] projetos = { "projeto1", "projeto2", "projeto3" };
        static readonly String[] pessoas = { "Christian", "Leonora", "Larissa", "Gustavo", "victor", "Gabriel", "Leandro" };
        static readonly String[] habilidades = { "Java", "C#", "c++", "Python", "PHP", "JavaScript" };
        static readonly String[] equipes = { "equipes1", "equipes2", "equipes3" };

        private Double[,] X = new Double[equipes.Length, projetos.Length];
        private Double[,] defasagem = new Double[projetos.Length, habilidades.Length];

        int[,] projetoHabilidade = {
            {0,1,1,0,0,0},
            {1,0,0,0,0,0},
            {0,0,0,0,1,1}
        };

        Double[] orcamento = { 30000, 15000, 45000 };

        int[] custoPessoa = { 24, 15, 53, 64, 12, 43, 12 };

        int[,] membroHabilidade = {
            {1,1,0,1,0,0},
            {0,0,1,0,1,1},
            {0,1,0,1,0,1},
            {1,1,1,1,1,1},
            {0,1,0,1,0,1},
            {0,0,0,0,0,0},
            {1,0,1,0,1,0}
        };

        int[] qtdeProjetosMembro = { 3, 7, 5, 2, 4, 5, 8, 2, 9 };

        public void GetObjetiva()
        {
            try
            {
                // Cria variáveis de decisão
                GRBVar[,] x = new GRBVar[pessoas.Length, projetos.Length];
                for (int i = 0; i < pessoas.Length; i++)
                    for (int j = 0; j < projetos.Length; j++)
                        x[i, j] = model.AddVar(0, 1, 0, GRB.BINARY, "");

                //Dqs
                GRBVar[,] defasagem = new GRBVar[projetos.Length, habilidades.Length];
                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                        defasagem[i, j] = model.AddVar(0, GRB.INFINITY, 0, GRB.CONTINUOUS, "");

                GRBVar E = new GRBVar();
                E = model.AddVar(0, GRB.INFINITY, 0, GRB.CONTINUOUS, "");


                // Criando restrições
                GRBLinExpr c1;

                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                    {
                        c1 = new GRBLinExpr();
                        c1 = projetoHabilidade[i, j];
                        for (int k = 0; k < pessoas.Length; k++)
                        {
                            c1 = c1 - x[k, i] * membroHabilidade[k, j];
                        }
                        model.AddConstr(defasagem[i, j], GRB.GREATER_EQUAL, c1, "");

                    }

                GRBLinExpr c2;

                for (int i = 0; i < projetos.Length; i++)
                {
                    c2 = new GRBLinExpr();
                    for (int j = 0; j < pessoas.Length; j++)
                    {
                        c2 = c2 + x[j, i] * custoPessoa[j];
                    }
                    model.AddConstr(c2, GRB.LESS_EQUAL, orcamento[i], "");
                }


                for (int i = 0; i < habilidades.Length; i++)
                {
                    for (int j = 0; j < projetos.Length - 1; j++)
                    {
                        for (int k = j + 1; k < projetos.Length; k++)
                        {
                            model.AddConstr(E, GRB.GREATER_EQUAL, defasagem[j, i] - defasagem[k, i], "");
                            model.AddConstr(E, GRB.GREATER_EQUAL, -defasagem[j, i] + defasagem[k, i], "");
                        }
                    }
                }

                //Criando função objetivo
                model.ModelSense = GRB.MINIMIZE;
                GRBLinExpr funcaoobj = new GRBLinExpr();

                for (int i = 0; i < projetos.Length; i++)
                {
                    for (int j = 0; j < habilidades.Length; j++)
                    {
                        funcaoobj = funcaoobj + 1000 * defasagem[i, j];
                    }
                }

                funcaoobj = funcaoobj + 1000 * E;

                model.SetObjective(funcaoobj);
                model.Optimize();

                for (int i = 0; i < pessoas.Length; i++)
                    for (int j = 0; j < projetos.Length; j++)
                        this.X[i, j] = x[i, j].X;

                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                        this.defasagem[i, j] = defasagem[i, j].X;

                this.E = E.X;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Double getValorZ()
        {
            return this.valorZ;
        }

        public Double[,] getX()
        {
            return this.X;
        }

        public Double[,] getDefasagem()
        {
            return this.defasagem;
        }

        public Double getE()
        {
            return this.E;
        }
    }
}
