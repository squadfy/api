CREATE DATABASE IF NOT EXSISTS SQUADFY;

USE SQUADFY;

CREATE TABLE Habilidade (
    `Id`        INT           AUTO_INCREMENT  NOT NULL,
    `Descricao` VARCHAR (100) NOT NULL
);
 
CREATE TABLE Historico (
    `Id`         INT           AUTO_INCREMENT  NOT NULL,
    `Metodo`     VARCHAR (100) NOT NULL,
    `Controller` VARCHAR (100) NOT NULL,
    `HttpStatus` INT           NULL,
    `Descricao`  VARCHAR (100) NOT NULL,
    `DataInicio` DATETIME(3)      NOT NULL,
    `DataFim`    DATETIME(3)      NOT NULL,
    `Request`    LONGTEXT  NULL,
    `Response`   LONGTEXT  NULL,
    `Exception`  LONGTEXT  NULL,
    `Usuario`    VARCHAR (20)  NULL,
    CONSTRAINT `PK_Historico` PRIMARY KEY (`Id` ASC)
);
 
CREATE TABLE RelTarefaHabilidade (
    `IdTarefa`     INT NOT NULL,
    `IdHabilidade` INT NOT NULL,
    PRIMARY KEY (`IdTarefa` ASC, `IdHabilidade` ASC)
);
 
CREATE TABLE RelTarefaUsuario (
    `Id`                   INT             AUTO_INCREMENT  NOT NULL,
    `Descricao`            VARCHAR (100)   NOT NULL,
    `Orcamento`            NUMERIC (18, 2) NOT NULL,
    `PESO`                 INT             NOT NULL,
    `DataEntrega`          DATETIME(3)        NOT NULL,
    `QuantidadeUsuario`    INT             NOT NULL,
    `QuantidadeMinUsuario` INT             NOT NULL,
    `QuantidadeMxUsuario`  INT             NOT NULL,
    CONSTRAINT `PK_Tarefa` PRIMARY KEY (`Id` ASC)
);

CREATE TABLE RelUsuarioHabilidade (
    `IdUsuario`    INT NOT NULL,
    `IdHabilidade` INT NOT NULL,
    PRIMARY KEY (`IdHabilidade` ASC, `IdUsuario` ASC)
);

CREATE TABLE Tarefa (
    `Id`                   INT             AUTO_INCREMENT  NOT NULL,
    `Descricao`            VARCHAR (500)   NOT NULL,
    `Orcamento`            NUMERIC (18, 2) NOT NULL,
    `Peso`                 INT             NOT NULL,
    `DataEntrega`          DATETIME(3)        NOT NULL,
    `QuantidadeUsuario`    INT             NOT NULL,
    `QuantidadeMinUsuario` INT             NOT NULL,
    `QuantidadeMxUsuario`  INT             NOT NULL,
    PRIMARY KEY (`Id` ASC)
);
 
CREATE TABLE TipoUsuario (
    `Id`        INT           AUTO_INCREMENT  NOT NULL,
    `Descricao` VARCHAR (100) NOT NULL,
    PRIMARY KEY (`Id` ASC)
);

CREATE TABLE Usuario (
    `Id`            INT             AUTO_INCREMENT  NOT NULL,
    `Nome`          VARCHAR (100)   NOT NULL,
    `Login`         VARCHAR (20)    NOT NULL,
    `Senha`         VARCHAR (500)   NOT NULL,
    `Salario`       NUMERIC (18, 2) NOT NULL,
    `IdTipoUsuario` INT             NOT NULL,
    PRIMARY KEY (`Id` ASC)
);

 
ALTER TABLE `dbo`.`Historico`
    ADD DEFAULT (now()) FOR `DataFim`;


ALTER TABLE `dbo`.`Usuario`
    ADD CONSTRAINT `FK_Usuario_TipoUsuario` FOREIGN KEY (`IdTipoUsuario`) REFERENCES TipoUsuario (`Id`);

