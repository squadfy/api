﻿using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;

namespace DataSquadFy.Repository
{
    public class HistoricoRepository : BaseRepository<Historico>, IHistoricoRepository
    {

    }
}