﻿using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;

namespace DataSquadFy.Repository
{
    public class RelTarefaHabilidadeRepository : BaseRepository<RelTarefaHabilidade>, IRelTarefaHabilidadeRepository
    {

    }
}