﻿using Dapper;
using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DataSquadFy.Repository
{
    public class TarefaRepository : BaseRepository<Tarefa>, ITarefaRepository
    {
        public void PopularConnectionString(string connectionString)
        {
            ConnectionString = connectionString;
        }

        const string QueryGet = @"select ta.*, pr.Id, pr.Nome, pr.Descricao, us.* from Tarefa ta 
                                    inner join Projeto pr on pr.Id = ta.IdProjeto
                                    left join RelTarefaUsuario rtu on rtu.IdTarefa = ta.Id
                                    left join Usuario us on us.Id = rtu.IdUsuario";
        public override IEnumerable<Tarefa> GetAll()
        {
            IEnumerable<Tarefa> tarefas = null;
            Tarefa tarefa = null;
            var lookup = new Dictionary<int, Tarefa>();
            using (var db = new SqlConnection(ConnectionString))
            {
                tarefas = db.Query<Tarefa, Projeto, Usuario, Tarefa>(QueryGet, map: (a, b, c) =>
                {
                    if (!lookup.TryGetValue(a.Id.Value, out tarefa))
                    {
                        a.Projeto = b;
                        tarefa = a;
                        lookup.Add(a.Id.Value, tarefa);
                    }

                    if (c != null)
                    {
                        if (tarefa.Usuarios == null)
                            tarefa.Usuarios = new List<Usuario>();

                        if (!tarefa.Usuarios.Any(ha => ha.Id == c.Id))
                            tarefa.Usuarios.Add(c);
                    }

                    return a;
                }, null, splitOn: "Id");
            }

            return lookup.Select(a => a.Value);
        }

        public override Tarefa GetById(int id)
        {
            Tarefa tarefa = null;
            var lookup = new Dictionary<int, Tarefa>();
            var q = $"{QueryGet} where ta.Id = @id";
            using (var db = new SqlConnection(ConnectionString))
            {
                return db.Query<Tarefa, Projeto, Usuario, Tarefa>(QueryGet, map: (a, b, c) =>
                {
                    if (!lookup.TryGetValue(a.Id.Value, out tarefa))
                    {
                        a.Projeto = b;
                        tarefa = a;
                        lookup.Add(a.Id.Value, tarefa);
                    }

                    if (c != null)
                    {
                        if (tarefa.Usuarios == null)
                            tarefa.Usuarios = new List<Usuario>();

                        if (!tarefa.Usuarios.Any(ha => ha.Id == c.Id))
                            tarefa.Usuarios.Add(c);
                    }

                    return a;
                }, new { id = id }, splitOn: "Id").FirstOrDefault();
            }
        }

        public override int Insert(Tarefa entity)
        {
            int idTarefa = 0;
            using (var db = new SqlConnection(ConnectionString))
            {
                idTarefa = base.Insert(entity);

                foreach (var item in entity.Usuarios)
                {
                    db.Execute("insert into RelTarefaUsuario values (@IdUsuario, @IdTarefa)", new { IdUsuario = item.Id, IdTarefa = idTarefa });
                }
            }

            return idTarefa;
        }

        public override bool Update(Tarefa entity)
        {
            using (var db = new SqlConnection(ConnectionString))
            {
                base.Update(entity);

                db.Execute("delete RelTarefaUsuario where IdTarefa = @IdTarefa", new
                {
                    IdTarefa = entity.Id
                });

                foreach (var item in entity.Usuarios)
                {
                    db.Execute("insert into RelTarefaUsuario values (@IdUsuario, @IdTarefa)", new
                    {
                        IdUsuario = item.Id,
                        IdTarefa = entity.Id
                    });
                }
            }

            return true;
        }
    }
}