﻿using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;

namespace DataSquadFy.Repository
{
    public class HabilidadeRepository : BaseRepository<Habilidade>, IHabilidadeRepository
    {

    }
}
