﻿using System.Collections.Generic;
using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace DataSquadFy.Repository
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        const string QueryGet = @"select us.*, tu.*, ha.* from Usuario us 
                                    inner join TipoUsuario tu on tu.Id = us.IdTipoUsuario
                                    left join RelUsuarioHabilidade ruh on ruh.IdUsuario = us.id
                                    left join Habilidade ha on ha.Id = ruh.IdHabilidade";
        public override IEnumerable<Usuario> GetAll()
        {
            IEnumerable<Usuario> usuarios = null;
            Usuario usuario = null;
            var lookup = new Dictionary<int, Usuario>();
            using (var db = new SqlConnection(ConnectionString))
            {
                usuarios = db.Query<Usuario, TipoUsuario, Habilidade, Usuario>(QueryGet, map: (a, b, c) =>
                {
                    if (!lookup.TryGetValue(a.Id.Value, out usuario))
                    {
                        a.TipoUsuario = b;
                        usuario = a;
                        lookup.Add(a.Id.Value, usuario);
                    }

                    if (c != null)
                    {
                        if (usuario.Habilidades == null)
                            usuario.Habilidades = new List<Habilidade>();

                        if (!usuario.Habilidades.Any(ha => ha.Id == c.Id))
                            usuario.Habilidades.Add(c);
                    }

                    return a;
                }, null, splitOn: "Id");
            }

            return lookup.Select(a => a.Value);
        }

        public override Usuario GetById(int id)
        {
            //IEnumerable<Usuario> usuarios = null;
            Usuario usuario = null;
            var lookup = new Dictionary<int, Usuario>();
            var q = $"{QueryGet} where us.Id = @id";
            using (var db = new SqlConnection(ConnectionString))
            {
                return db.Query<Usuario, TipoUsuario, Habilidade, Usuario>(q, map: (a, b, c) =>
                {
                    if (!lookup.TryGetValue(a.Id.Value, out usuario))
                    {
                        a.TipoUsuario = b;
                        usuario = a;
                        lookup.Add(a.Id.Value, usuario);
                    }

                    if (c != null)
                    {
                        if (usuario.Habilidades == null)
                            usuario.Habilidades = new List<Habilidade>();

                        if (!usuario.Habilidades.Any(ha => ha.Id == c.Id))
                            usuario.Habilidades.Add(c);
                    }

                    return a;
                }, new { id = id }, splitOn: "Id").FirstOrDefault();
            }
        }

        public override int Insert(Usuario entity)
        {
            RelUsuarioHabilidadeRepository relUsuarioHabilidade = new RelUsuarioHabilidadeRepository();
            int idUsuario = 0;
            using (var db = new SqlConnection(ConnectionString))
            {
                idUsuario = base.Insert(entity);

                foreach (var item in entity.Habilidades)
                {
                    db.Execute("insert into RelUsuarioHabilidade values (@IdUsuario, @IdHabilidade)", new { IdUsuario = idUsuario, IdHabilidade = item.Id });
                }
            }

            return idUsuario;
        }

        public override bool Update(Usuario entity)
        {
            RelUsuarioHabilidadeRepository relUsuarioHabilidade = new RelUsuarioHabilidadeRepository();
            using (var db = new SqlConnection(ConnectionString))
            {
                base.Update(entity);

                db.Execute("delete RelUsuarioHabilidade where IdUsuario = @IdUsuario", new
                {
                    IdUsuario = entity.Id
                });

                foreach (var item in entity.Habilidades)
                {
                    db.Execute("insert into RelUsuarioHabilidade values (@IdUsuario, @IdHabilidade)", new
                    {
                        IdUsuario = entity.Id,
                        IdHabilidade = item.Id
                    });
                }
            }

            return true;
        }
    }
}