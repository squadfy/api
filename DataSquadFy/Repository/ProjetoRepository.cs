﻿using Dapper;
using DataSquadFy.Repository.Base;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DataSquadFy.Repository
{
    public class ProjetoRepository : BaseRepository<Projeto>, IProjetoRepository
    {
        const string QueryGet = @"select pr.Id, pr.Nome, pr.Descricao, ta.*, us.*, tp.*, ha.* from projeto pr
                                    left join Tarefa ta on ta.IdProjeto = pr.Id
                                    left join RelTarefaUsuario rtu on rtu.IdTarefa = ta.Id
                                    left join Usuario us on us.Id = rtu.IdUsuario
                                    left join TipoUsuario tp on tp.Id = us.IdTipoUsuario
                                    left join RelUsuarioHabilidade ruh on ruh.IdUsuario = us.Id
                                    left join Habilidade ha on ha.Id = ruh.IdHabilidade";
        public override IEnumerable<Projeto> GetAll()
        {
            IEnumerable<Projeto> projetos = null;
            Projeto projeto = null;
            var lookup = new Dictionary<int, Projeto>();
            using (var db = new SqlConnection(ConnectionString))
            {
                projetos = db.Query<Projeto, Tarefa, Usuario, TipoUsuario, Habilidade, Projeto>(QueryGet, map: (a, b, c, d, e) =>
                {
                    if (!lookup.TryGetValue(a.Id, out projeto))
                        lookup.Add(a.Id, projeto = a);

                    if (b != null)
                    {
                        if (projeto.Tarefas == null)
                            projeto.Tarefas = new List<Tarefa>();

                        if (!projeto.Tarefas.Any(ta => ta.Id == b.Id))
                            projeto.Tarefas.Add(b);

                        if (c != null)
                        {
                            if (projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios == null)
                            {
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios = new List<Usuario>();
                                c.TipoUsuario = d;
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id)?.Usuarios.Add(c);
                            }
                        }

                        if (e != null)
                        {
                            if (projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades == null)
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades = new List<Habilidade>();

                            projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades.Add(e);
                        }
                    }

                    return a;
                }, null, splitOn: "Id");
            }

            return lookup.Select(a => a.Value);
        }

        public override Projeto GetById(int id)
        {
            Projeto projeto = null;
            var lookup = new Dictionary<int, Projeto>();
            var q = $"{QueryGet} where pr.Id = @id";
            using (var db = new SqlConnection(ConnectionString))
            {
                return db.Query<Projeto, Tarefa, Usuario, TipoUsuario, Habilidade, Projeto>(QueryGet, map: (a, b, c, d, e) =>
                {
                    if (!lookup.TryGetValue(a.Id, out projeto))
                        lookup.Add(a.Id, projeto = a);

                    if (b != null)
                    {
                        if (projeto.Tarefas == null)
                            projeto.Tarefas = new List<Tarefa>();

                        if (!projeto.Tarefas.Any(ta => ta.Id == b.Id))
                            projeto.Tarefas.Add(b);

                        if (c != null)
                        {
                            if (projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios == null)
                            {
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios = new List<Usuario>();
                                c.TipoUsuario = d;
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id)?.Usuarios.Add(c);
                            }
                        }

                        if (e != null)
                        {
                            if (projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades == null)
                                projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades = new List<Habilidade>();

                            projeto.Tarefas.FirstOrDefault(ta => ta.Id == b.Id).Usuarios.FirstOrDefault(u => u.Id == c.Id).Habilidades.Add(e);
                        }
                    }

                    return a;
                }, new { id = id }, splitOn: "Id").FirstOrDefault();
            }
        }

        public override int Insert(Projeto entity)
        {
            TarefaRepository tarefaRepository = new TarefaRepository();
            int idProjeto = 0;
            using (var db = new SqlConnection(ConnectionString))
            {
                tarefaRepository.PopularConnectionString(ConnectionString);
                idProjeto = base.Insert(entity);

                foreach (var item in entity.Tarefas)
                {
                    item.IdProjeto = idProjeto;
                    tarefaRepository.Insert(item);
                }
            }

            return idProjeto;
        }

        public override bool Update(Projeto entity)
        {
            TarefaRepository tarefaRepository = new TarefaRepository();
            using (var db = new SqlConnection(ConnectionString))
            {
                tarefaRepository.PopularConnectionString(ConnectionString);
                base.Update(entity);

                foreach (var item in entity.Tarefas)
                {
                    item.IdProjeto = entity.Id;
                    tarefaRepository.Update(item);
                }
            }

            return true;
        }
    }
}