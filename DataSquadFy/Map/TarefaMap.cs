﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class TarefaMap : DommelEntityMap<Tarefa>
    {
        public TarefaMap()
        {
            ToTable("Tarefa");
            Map(x => x.Id).IsKey().IsIdentity();
        }
    }
}