﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class RelTarefaUsuarioMap : DommelEntityMap<RelTarefaUsuario>
    {
        public RelTarefaUsuarioMap()
        {
            ToTable("RelTarefaUsuario");
            Map(x => x.IdTarefa).IsKey();
            Map(x => x.IdUsuario).IsKey();
        }
    }
}