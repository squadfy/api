﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class UsuarioMap : DommelEntityMap<Usuario>
    {
        public UsuarioMap()
        {
            ToTable("Usuario");
            Map(x => x.Id).IsKey().IsIdentity();
        }
    }
}