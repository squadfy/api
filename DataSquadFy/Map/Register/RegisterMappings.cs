﻿using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;

namespace DataSquadFy.Map.Register
{
    public class RegisterMappings
    {
        public static void Register()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new UsuarioMap());
                config.AddMap(new HabilidadeMap());
                config.AddMap(new HistoricoMap());
                config.AddMap(new RelTarefaHabilidadeMap());
                config.AddMap(new RelTarefaUsuarioMap());
                config.AddMap(new RelUsuarioHabilidadeMap());
                config.AddMap(new TarefaMap());
                config.AddMap(new TipoUsuarioMap());
                config.AddMap(new ProjetoMap());
                config.ForDommel();
            });
        }
    }
}
