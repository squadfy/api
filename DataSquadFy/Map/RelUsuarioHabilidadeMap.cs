﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class RelUsuarioHabilidadeMap : DommelEntityMap<RelUsuarioHabilidade>
    {
        public RelUsuarioHabilidadeMap()
        {
            ToTable("RelUsuarioHabilidade");
            Map(x => x.IdHabilidade).IsKey();
            Map(x => x.IdUsuario).IsKey();
        }
    }
}