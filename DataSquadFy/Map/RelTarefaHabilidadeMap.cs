﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class RelTarefaHabilidadeMap : DommelEntityMap<RelTarefaHabilidade>
    {
        public RelTarefaHabilidadeMap()
        {
            ToTable("RelTarefaHabilidade");
            Map(x => x.IdHabilidade).IsKey();
            Map(x => x.IdTarefa).IsKey();
        }
    }
}