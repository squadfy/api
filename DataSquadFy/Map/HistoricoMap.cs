﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class HistoricoMap : DommelEntityMap<Historico>
    {
        public HistoricoMap()
        {
            ToTable("Historico");
            Map(x => x.Id).IsKey().IsIdentity();
            Map(x => x.DataFim).Ignore();
        }
    }
}