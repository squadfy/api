﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class ProjetoMap : DommelEntityMap<Projeto>
    {
        public ProjetoMap()
        {
            ToTable("Projeto");
            Map(x => x.Id).IsKey().IsIdentity();
        }
    }
}