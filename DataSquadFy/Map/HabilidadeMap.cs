﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    class HabilidadeMap : DommelEntityMap<Habilidade>
    {
        public HabilidadeMap()
        {
            ToTable("Habilidade");
            Map(x => x.Id).IsKey().IsIdentity();
        }
    }
}