﻿using Dapper.FluentMap.Dommel.Mapping;
using DomainSquadFy.Entity;

namespace DataSquadFy.Map
{
    public class TipoUsuarioMap : DommelEntityMap<TipoUsuario>
    {
        public TipoUsuarioMap()
        {
            ToTable("TipoUsuario");
            Map(x => x.Id).IsKey().IsIdentity();
        }
    }
}