﻿using System;
using System.Collections.Generic;

namespace DomainSquadFy.Entity
{
    public class Tarefa
    {
        public int? Id { get; set; }
        public string Descricao { get; set; }
        public double Orcamento { get; set; }
        public int QuantidadeUsuario { get; set; }
        public int QuantidadeMxUsuario { get; set; }
        public int IdProjeto { get; set; }
        public bool Status { get; set; }

        public virtual List<Usuario> Usuarios { get; set; }
        public virtual Projeto Projeto { get; set; }
    }
}