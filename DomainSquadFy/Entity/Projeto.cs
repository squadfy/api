﻿using System.Collections.Generic;

namespace DomainSquadFy.Entity
{
    public class Projeto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public virtual List<Tarefa> Tarefas { get; set; }
    }
}