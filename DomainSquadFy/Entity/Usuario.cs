﻿using System.Collections.Generic;

namespace DomainSquadFy.Entity
{
    public class Usuario
    {
        public int? Id { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public double Salario { get; set; }
        public string Cargo { get; set; }
        public int QtdProjeto { get; set; }
        public int IdTipoUsuario { get; set; }
        public virtual TipoUsuario TipoUsuario { get; set; }
        public virtual List<Habilidade> Habilidades { get; set; }
    }
}