﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainSquadFy.Entity
{
    public class Habilidade
    {
        public int? Id { get; set; }
        public string Descricao { get; set; }
    }
}
