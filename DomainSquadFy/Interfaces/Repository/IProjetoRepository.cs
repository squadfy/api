﻿using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository.Base;

namespace DomainSquadFy.Interfaces.Repository
{
    public interface IProjetoRepository : IBaseRepository<Projeto>
    {
    }
}