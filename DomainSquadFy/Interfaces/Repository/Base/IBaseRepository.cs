﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DomainSquadFy.Interfaces.Repository.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        int Insert(TEntity entity);

        bool Update(TEntity entity);

        bool Delete(int id);

        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate);
    }
}