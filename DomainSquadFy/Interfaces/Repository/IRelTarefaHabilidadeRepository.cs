﻿using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository.Base;

namespace DomainSquadFy.Interfaces.Repository
{
    public interface IRelTarefaHabilidadeRepository : IBaseRepository<RelTarefaHabilidade>
    {
    }
}
