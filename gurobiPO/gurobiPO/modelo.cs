﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gurobiPO
{
    using System;
    using Gurobi;
    class modelo
    {
        

        static void Main()
        {
            try
            {
                String[] projetos =
                {
                    "projeto1","projeto2","projeto3"
                };
                String[] equipes =
                {
                    "Christian", "Leonora", "Larissa", "Gustavo", "victor", "Gabriel", "Leandro"
                };
                String[] habilidades =
                {
                    "Java", "C#", "c++", "Python", "PHP", "JavaScript"
                };

                int[,] projetoHabilidade =
                {
                    {0,1,1,0,0,0},
                    {1,0,0,0,0,0},
                    {0,0,0,0,1,1}
                };

                Double[] orcamento =
                {
                   30000, 15000, 45000
                };

                int[] custoPessoa =
                {
                    24, 15, 53, 64, 12, 43, 12
                };
                
                int[,] membroHabilidade =
                {
                    {1,1,0,1,0,0},
                    {0,0,1,0,1,1},
                    {0,1,0,1,0,1},
                    {1,1,1,1,1,1},
                    {0,1,0,1,0,1},
                    {0,0,0,0,0,0},
                    {1,0,1,0,1,0}
                };

                int[] qtdeProjetosMembro =
                {
                    3,7,5,2,4,5,8,2,9
                };

                

                // Criando Modelo e Ambiente
                GRBEnv env = new GRBEnv("modelo.log");
                GRBModel model = new GRBModel(env);

                // Criando variáveis de decisão
                //Xeq
                GRBVar[,] x = new GRBVar[equipes.Length, projetos.Length];
                for (int i = 0; i < equipes.Length; i++)
                    for (int j = 0; j < projetos.Length; j++)
                        x[i, j] = model.AddVar(0, 1, 0, GRB.BINARY,"");

                //Dqs
                GRBVar[,] defasagem = new GRBVar[projetos.Length, habilidades.Length];
                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                        defasagem[i, j] = model.AddVar(0, GRB.INFINITY, 0, GRB.CONTINUOUS, "");

                GRBVar E = new GRBVar();
                E = model.AddVar(0, GRB.INFINITY, 0, GRB.CONTINUOUS, "");


                // Criando restrições
                GRBLinExpr c1;

                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                    {
                        c1 = new GRBLinExpr();
                        c1 = projetoHabilidade[i, j];
                        for (int k = 0; k < equipes.Length; k++)
                        {
                            c1 = c1 - x[k, i] * membroHabilidade[k, j];
                        }
                        model.AddConstr(defasagem[i, j], GRB.GREATER_EQUAL, c1, "");

                    }

                GRBLinExpr c2;

                for (int i = 0; i < projetos.Length; i++)
                {
                    c2 = new GRBLinExpr();
                    for (int j = 0; j < equipes.Length; j++)
                    {
                        c2 = c2 + x[j, i] * custoPessoa[j];
                    }
                    model.AddConstr(c2, GRB.LESS_EQUAL, orcamento[i], "");
                }


                for (int i = 0; i < habilidades.Length; i++)
                {
                    for (int j = 0; j < projetos.Length - 1; j++)
                    {
                        for (int k = j + 1; k < projetos.Length; k++)
                        {
                            model.AddConstr(E, GRB.GREATER_EQUAL, defasagem[j,i] - defasagem[k,i], "");
                            model.AddConstr(E, GRB.GREATER_EQUAL, - defasagem[j, i] + defasagem[k, i], "");
                        }
                    }
                }

                //Criando função objetivo
                model.ModelSense = GRB.MINIMIZE;
                GRBLinExpr funcaoobj = new GRBLinExpr();
                
                for (int i = 0; i < projetos.Length; i++)
                {
                    for (int j = 0; j < habilidades.Length; j++)
                    {
                        funcaoobj = funcaoobj + 1000 * defasagem[i, j];
                    }
                }

                funcaoobj = funcaoobj + 1000 * E;

                model.SetObjective(funcaoobj);
                model.Optimize();


                Double valorZ = model.ObjVal;
                Double[,] x1 = new Double[equipes.Length, projetos.Length];
                Double[,] defasagem1 = new Double[projetos.Length, habilidades.Length];
                Double E1 = 0;

                for (int i = 0; i < equipes.Length; i++)
                    for (int j = 0; j < projetos.Length; j++)
                        x1[i, j] = x[i, j].X;

                for (int i = 0; i < projetos.Length; i++)
                    for (int j = 0; j < habilidades.Length; j++)
                        defasagem1[i, j] = defasagem[i, j].X;

                E1 = E.X;
            }
            catch (GRBException e)
            {
                Console.WriteLine("Error code: " + e.ErrorCode + ". " + e.Message);
            }
        }

        private static int[,] CalculaDefasagemProjeto(int[,] projetoHabilidade, int[,] alocacaoMembros, int qtdePessoas, int[,] membroHabilidade)
        {
            int[,] rtrn = new int[3, 6];

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 6; j++)
                    rtrn[i, j] = projetoHabilidade[i, j] - SomaPesoProjeto(alocacaoMembros, i, qtdePessoas, membroHabilidade, j);

            return rtrn;
        }

        private static int SomaPesoProjeto(int[,] alocacaoMembros, int indiceProjeto, int qtdePessoas, int[,] membroHabilidade, int indiceHabilidade)
        {
            int pesoProjeto = 0;

            for (int i = 0; i < qtdePessoas; i++)
                pesoProjeto += alocacaoMembros[i, indiceProjeto] * membroHabilidade[i, indiceHabilidade];

            return pesoProjeto;
        }
    }
}