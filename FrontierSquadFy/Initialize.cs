﻿using DataSquadFy.Map.Register;
using DataSquadFy.Repository;
using DomainSquadFy.Interfaces.Repository;
using SimpleInjector;

namespace FrontierSquadFy
{
    public static class Initialize
    {
        public static Container ConfigInitialize()
        {
            RegisterMappings.Register();
            // 1. Create a new Simple Injector container
            var container = new Container();

            // 2. Configure the container (register)
            // See below for more configuration examples
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Singleton);
            container.Register<IHabilidadeRepository, HabilidadeRepository>(Lifestyle.Singleton);
            container.Register<ITarefaRepository, TarefaRepository>(Lifestyle.Singleton);
            container.Register<IRelTarefaHabilidadeRepository, RelTarefaHabilidadeRepository>(Lifestyle.Singleton);
            container.Register<IRelTarefaUsuarioRepository, RelTarefaUsuarioRepository>(Lifestyle.Singleton);
            container.Register<IRelUsuarioHabilidadeRepository, RelUsuarioHabilidadeRepository>(Lifestyle.Singleton);
            container.Register<IHistoricoRepository, HistoricoRepository>(Lifestyle.Singleton);
            container.Register<ITipoUsuarioRepository, TipoUsuarioRepository>(Lifestyle.Singleton);
            container.Register<IProjetoRepository, ProjetoRepository>(Lifestyle.Singleton);

            // 3. Optionally verify the container's configuration.
            container.Verify();

            return container;
        }
    }
}
