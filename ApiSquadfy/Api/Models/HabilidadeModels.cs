﻿using System.ComponentModel.DataAnnotations;

namespace ApiSquadfy.Api.Models
{
    public class HabilidadeModels
    {
        /// <summary>
        /// Descrição da habilidade
        /// </summary>
        [Required(ErrorMessage = "O campo 'Descricao' é de preenchimento obrigatório.")]
        public string Descricao { get; set; }
    }
}
