﻿namespace ApiSquadfy.Api.Models
{
    public class RelTarefaUsuarioModels
    {
        public int IdTarefa { get; set; }
        public int IdUsuario { get; set; }
    }
}
