﻿using DomainSquadFy.Entity;
using System;
using System.Collections.Generic;

namespace ApiSquadfy.Api.Models
{
    public class TarefaModels
    {
        public string Descricao { get; set; }
        public double Orcamento { get; set; }
        public int QuantidadeUsuario { get; set; }
        public int QuantidadeMxUsuario { get; set; }
        public int IdProjeto { get; set; }
        public bool Status { get; set; }
        public List<Usuario> Usuarios { get; set; }
    }
}
