﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiSquadfy.Api.Models
{
    public class RelTarefaHabilidadeModels
    {
        public int IdTarefa { get; set; }
        public int IdHabilidade { get; set; }
    }
}
