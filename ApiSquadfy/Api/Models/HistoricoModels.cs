﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiSquadfy.Api.Models
{
    public class HistoricoModels
    {
        public int Id { get; set; }
        public string Metodo { get; set; }
        public string Controller { get; set; }
        public int HttpStatus { get; set; }
        public string Descricao { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string Exception { get; set; }
        public string Usuario { get; set; }
    }
}
