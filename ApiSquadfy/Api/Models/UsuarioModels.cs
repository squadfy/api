﻿using DomainSquadFy.Entity;
using System.Collections.Generic;

namespace ApiSquadfy.Api.Models
{
    public class UsuarioModels
    {
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public double Salario { get; set; }
        public int IdTipoUsuario { get; set; }
        public int QtdProjeto { get; set; }
        public string Cargo { get; set; }
        public List<Habilidade> Habilidades { get; set; }
    }
}
