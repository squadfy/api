﻿namespace ApiSquadfy.Api.Models
{
    public class RelUsuarioHabilidadeModels
    {
        public int IdUsuario { get; set; }
        public int IdHabilidade { get; set; }
    }
}
