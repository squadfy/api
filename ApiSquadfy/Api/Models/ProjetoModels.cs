﻿using DomainSquadFy.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiSquadfy.Api.Models
{
    public class ProjetoModels
    {
        [Required(ErrorMessage = "O campo 'Nome' é de preenchimento obrigatório.")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O campo 'Descricao' é de preenchimento obrigatório.")]
        public string Descricao { get; set; }
        public List<TarefaModels> Tarefas { get; set; }
    }
}
