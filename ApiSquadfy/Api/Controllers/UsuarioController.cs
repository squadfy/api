﻿using ApiSquadfy.Api.Models;
using AutoMapper;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        IMapper _mapper;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly ITipoUsuarioRepository _tipoUsuarioRepository;

        public UsuarioController(IUsuarioRepository usuarioRepository, ITipoUsuarioRepository tipoUsuarioRepository, IMapper mapper)
        {
            _usuarioRepository = usuarioRepository;
            _tipoUsuarioRepository = tipoUsuarioRepository;
            _mapper = mapper;
        }

        // GET: api/Usuario
        [HttpGet]
        [ProducesResponseType(typeof(List<Usuario>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Usuario>>> Get()
        {
            List<Usuario> usuarios = this._usuarioRepository.GetAll().ToList();
            if (usuarios.Any())
            {
                usuarios.ForEach(u => u.TipoUsuario = _tipoUsuarioRepository.GetById(u.IdTipoUsuario));
                return usuarios;
            }
            return NotFound();
        }

        [ProducesResponseType(typeof(Usuario), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/TipoUsuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> GetById(int id)
        {
            Usuario usuario;
            usuario = this._usuarioRepository.GetById(id);
            if (usuario != null)
            {
                usuario.TipoUsuario = _tipoUsuarioRepository.GetById(usuario.IdTipoUsuario);
                return usuario;
            }
            return NotFound();
        }

        [HttpGet("login/{login}/{senha}")]
        [ProducesResponseType(typeof(List<Usuario>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Usuario>> Login(string login, string senha)
        {
            Usuario usuario = this._usuarioRepository.GetList(a => a.Login == login && a.Senha == senha).FirstOrDefault();
            if (usuario != null)
            {
                usuario.TipoUsuario = _tipoUsuarioRepository.GetById(usuario.IdTipoUsuario);
                return usuario;
            }

            return NotFound();
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Usuario
        [HttpPost]
        public async Task<ActionResult<UsuarioModels>> Incluir([FromBody] UsuarioModels dados)
        {
            Usuario Usuario = _mapper.Map<Usuario>(dados);
            return Ok(_usuarioRepository.Insert(Usuario));

        }

        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Usuario/5
        [HttpPut("{id}")]
        public async Task<ActionResult<UsuarioModels>> Alterar([FromBody] UsuarioModels dados, int id)
        {
            Usuario Usuario = _mapper.Map<Usuario>(dados);
            Usuario.Id = id;
            return Ok(_usuarioRepository.Update(Usuario));

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _usuarioRepository.Delete(id);
            return Ok();
        }
    }
}