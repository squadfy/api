﻿using System;
using ApiSquadfy.Api.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ModelSquadfy;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModeloController : BaseController
    {

        public ModeloController()
        {
        }

        [HttpGet("/Defasagem", Name = "GetDefasagem")]
        [ProducesResponseType(typeof(Double[,]), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Double[,]> GetDefasagem()
        {
            GurobiModel gurobiModel = new GurobiModel();
            return gurobiModel.getDefasagem();
        }

        [HttpGet("/X", Name = "GetX")]
        [ProducesResponseType(typeof(Double[,]), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Double[,]> GetX()
        {
            GurobiModel gurobiModel = new GurobiModel();
            return gurobiModel.getX();
        }

        [HttpGet("/Z", Name = "GetZ")]
        [ProducesResponseType(typeof(Double), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Double> GetZ()
        {
            GurobiModel gurobiModel = new GurobiModel();
            return gurobiModel.getValorZ();
        }

        [HttpGet("/E", Name = "GetE")]
        [ProducesResponseType(typeof(Double), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Double> GetE()
        {
            GurobiModel gurobiModel = new GurobiModel();
            return gurobiModel.getE();
        }
    }
}
