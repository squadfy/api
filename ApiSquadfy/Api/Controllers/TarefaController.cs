﻿using ApiSquadfy.Api.Controllers.Base;
using ApiSquadfy.Api.Models;
using AutoMapper;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ModelSquadfy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarefaController : BaseController
    {
        IMapper _mapper;
        private readonly ITarefaRepository _tarefaRepository;

        public TarefaController(ITarefaRepository tarefaRepository, IMapper mapper)
        {
            _tarefaRepository = tarefaRepository;
            _mapper = mapper;
        }

        // GET: api/Tarefa
        [HttpGet]
        [ProducesResponseType(typeof(List<Tarefa>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Tarefa>>> Get()
        {
            List<Tarefa> tarefas = this._tarefaRepository.GetAll().ToList();
            if (tarefas.Any()) return tarefas;

            return NotFound();
        }

        [ProducesResponseType(typeof(Tarefa), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Tarefa/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tarefa>> GetById(int id)
        {
            Tarefa tarefa;
            tarefa = this._tarefaRepository.GetById(id);
            if (tarefa != null) return tarefa;

            return NotFound();
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Tarefa
        [HttpPost]
        public async Task<ActionResult<TarefaModels>> Incluir([FromBody] TarefaModels dados)
        {
            Tarefa tarefa = _mapper.Map<Tarefa>(dados);
            return Ok(_tarefaRepository.Insert(tarefa));

        }

        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Tarefa/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TarefaModels>> Alterar([FromBody] TarefaModels dados, int id)
        {
            Tarefa tarefa = _mapper.Map<Tarefa>(dados);
            tarefa.Id = id;
            return Ok(_tarefaRepository.Update(tarefa));

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _tarefaRepository.Delete(id);
            return Ok();
        }
    }
}