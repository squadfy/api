﻿using ApiSquadfy.Api.Models;
using AutoMapper;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoUsuarioController : ControllerBase
    {
        IMapper _mapper;
        private readonly ITipoUsuarioRepository _tipoUsuarioRepository;

        public TipoUsuarioController(ITipoUsuarioRepository tipoUsuarioRepository, IMapper mapper)
        {
            _tipoUsuarioRepository = tipoUsuarioRepository;
            _mapper = mapper;
        }

        // GET: api/TipoUsuario
        [HttpGet]
        [ProducesResponseType(typeof(List<TipoUsuario>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<TipoUsuario>>> Get()
        {
            List<TipoUsuario> TipoUsuarios = this._tipoUsuarioRepository.GetAll().ToList();
            if (TipoUsuarios.Any()) return TipoUsuarios;

            return NotFound();
        }

        //[HttpGet("{descricao}", Name = "GetByDescricao")]
        //[ProducesResponseType(typeof(List<TipoUsuario>), StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status403Forbidden)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public async Task<ActionResult<List<TipoUsuario>>> GetByDescricao(string descricao)
        //{
        //    List<TipoUsuario> TipoUsuarios = this._tipoUsuarioRepository.GetList(a => a.Descricao.Contains(descricao)).ToList();
        //    if (TipoUsuarios.Any()) return TipoUsuarios;

        //    return NotFound();
        //}

        [ProducesResponseType(typeof(TipoUsuario), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/TipoUsuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoUsuario>> GetById(int id)
        {
            TipoUsuario TipoUsuario;
            TipoUsuario = this._tipoUsuarioRepository.GetById(id);
            if (TipoUsuario != null) return TipoUsuario;

            return NotFound();
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/TipoUsuario
        [HttpPost]
        public async Task<ActionResult<TipoUsuarioModels>> Incluir([FromBody] TipoUsuarioModels dados)
        {
            TipoUsuario TipoUsuario = _mapper.Map<TipoUsuario>(dados);
            return Ok(_tipoUsuarioRepository.Insert(TipoUsuario));

        }

        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/TipoUsuario/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TipoUsuarioModels>> Alterar([FromBody] TipoUsuarioModels dados, int id)
        {
            TipoUsuario TipoUsuario = _mapper.Map<TipoUsuario>(dados);
            TipoUsuario.Id = id;
            return Ok(_tipoUsuarioRepository.Update(TipoUsuario));

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _tipoUsuarioRepository.Delete(id);
            return Ok();
        }
    }
}