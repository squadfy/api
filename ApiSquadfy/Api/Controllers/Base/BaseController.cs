﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiSquadfy.Api.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class BaseController : ControllerBase
    {
        
    }
}