﻿using ApiSquadfy.Api.Controllers.Base;
using ApiSquadfy.Api.Models;
using AutoMapper;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HabilidadeController : BaseController
    {
        IMapper _mapper;
        private readonly IHabilidadeRepository _habilidadeRepository;

        public HabilidadeController(IHabilidadeRepository habilidadeRepository, IMapper mapper)
        {
            _habilidadeRepository = habilidadeRepository;
            _mapper = mapper;
        }

        // GET: api/Habilidade
        [HttpGet]
        [ProducesResponseType(typeof(List<Habilidade>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Habilidade>>> Get()
        {
            List<Habilidade> habilidades = this._habilidadeRepository.GetAll().ToList();
            if (habilidades.Any()) return habilidades;

            return NotFound();
        }

        [HttpGet("descricao/{descricao}")]
        [ProducesResponseType(typeof(List<Habilidade>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Habilidade>>> GetByDescricao(string descricao)
        {
            List<Habilidade> habilidades = this._habilidadeRepository.GetList(a => a.Descricao.Contains(descricao)).ToList();
            if (habilidades.Any()) return habilidades;

            return NotFound();
        }

        [ProducesResponseType(typeof(Habilidade), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Habilidade/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Habilidade>> GetById(int id)
        {
            Habilidade habilidade;
            habilidade = this._habilidadeRepository.GetById(id);
            if (habilidade != null) return habilidade;

            return NotFound();
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Habilidade
        [HttpPost]
        public async Task<ActionResult<HabilidadeModels>> Incluir([FromBody] HabilidadeModels dados)
        {
            Habilidade habilidade = _mapper.Map<Habilidade>(dados);
            return Ok(_habilidadeRepository.Insert(habilidade));

        }

        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Habilidade/5
        [HttpPut("{id}")]
        public async Task<ActionResult<HabilidadeModels>> Alterar([FromBody] HabilidadeModels dados, int id)
        {
            Habilidade habilidade = _mapper.Map<Habilidade>(dados);
            habilidade.Id = id;
            return Ok(_habilidadeRepository.Update(habilidade));

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _habilidadeRepository.Delete(id);
            return Ok();
        }
    }
}