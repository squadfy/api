﻿using ApiSquadfy.Api.Controllers.Base;
using ApiSquadfy.Api.Models;
using AutoMapper;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSquadfy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoController : BaseController
    {
        IMapper _mapper;
        private readonly IProjetoRepository _projetoRepository;

        public ProjetoController(IProjetoRepository projetoRepository, IMapper mapper)
        {
            _projetoRepository = projetoRepository;
            _mapper = mapper;
        }
        [HttpGet]
        [ProducesResponseType(typeof(List<Projeto>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Projeto>>> Get()
        {
            List<Projeto> projeto = this._projetoRepository.GetAll().ToList();
            if (projeto.Any()) return projeto;

            return NotFound();
        }

        [ProducesResponseType(typeof(Projeto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Habilidade/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Projeto>> GetById(int id)
        {
            Projeto projeto;
            projeto = this._projetoRepository.GetById(id);
            if (projeto != null) return projeto;

            return NotFound();
        }

        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Habilidade
        [HttpPost]
        public async Task<ActionResult<ProjetoModels>> Incluir([FromBody] ProjetoModels dados)
        {
            Projeto projeto = _mapper.Map<Projeto>(dados);
            return Ok(_projetoRepository.Insert(projeto));

        }

        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Habilidade/5
        [HttpPut("{id}")]
        public async Task<ActionResult<ProjetoModels>> Alterar([FromBody] ProjetoModels dados, int id)
        {
            Projeto projeto = _mapper.Map<Projeto>(dados);
            projeto.Id = id;
            return Ok(_projetoRepository.Update(projeto));

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            _projetoRepository.Delete(id);
            return Ok();
        }
    }
}