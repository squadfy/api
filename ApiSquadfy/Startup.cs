﻿using ApiSquadfy.Api.Models;
using AutoMapper;
using DataSquadFy.Repository;
using DomainSquadFy.Entity;
using DomainSquadFy.Interfaces.Repository;
using FrontierSquadFy;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector.Integration.Web.Mvc;
using Swashbuckle.AspNetCore.Swagger;
using System.Web.Mvc;
using System.Web;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace ApiSquadfy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin", builder => builder.AllowAnyOrigin());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "SquadFy API", Version = "v1" });
            });

            Injection(services);

            // 4. Store the container for use by the application
            DependencyResolver.SetResolver(
                new SimpleInjectorDependencyResolver(Initialize.ConfigInitialize()));

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                AutoMapper(cfg);
            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }

        private void Injection(IServiceCollection services)
        {
            services.AddScoped<IHabilidadeRepository>(factory =>
            {
                return new HabilidadeRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IUsuarioRepository>(factory =>
            {
                return new UsuarioRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<ITarefaRepository>(factory =>
            {
                return new TarefaRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<ITipoUsuarioRepository>(factory =>
            {
                return new TipoUsuarioRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IRelUsuarioHabilidadeRepository>(factory =>
            {
                return new RelUsuarioHabilidadeRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IRelTarefaUsuarioRepository>(factory =>
            {
                return new RelTarefaUsuarioRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IRelTarefaHabilidadeRepository>(factory =>
            {
                return new RelTarefaHabilidadeRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IHistoricoRepository>(factory =>
            {
                return new HistoricoRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
            services.AddScoped<IProjetoRepository>(factory =>
            {
                return new ProjetoRepository() { ConnectionString = Configuration.GetConnectionString("SquadFyConn") };
            });
        }
        private static void AutoMapper(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Usuario, UsuarioModels>();
            cfg.CreateMap<UsuarioModels, Usuario>().ForMember(usuario => usuario.TipoUsuario, opt => opt.Ignore());

            cfg.CreateMap<Habilidade, HabilidadeModels>();
            cfg.CreateMap<HabilidadeModels, Habilidade>();

            cfg.CreateMap<TipoUsuario, TipoUsuarioModels>();
            cfg.CreateMap<TipoUsuarioModels, TipoUsuario>();

            cfg.CreateMap<Tarefa, TarefaModels>();
            cfg.CreateMap<TarefaModels, Tarefa>();

            cfg.CreateMap<Historico, HistoricoModels>();
            cfg.CreateMap<HistoricoModels, Historico>();

            cfg.CreateMap<RelTarefaHabilidade, RelTarefaHabilidadeModels>();
            cfg.CreateMap<RelTarefaHabilidadeModels, RelTarefaHabilidade>();

            cfg.CreateMap<RelTarefaUsuario, RelTarefaUsuarioModels>();
            cfg.CreateMap<RelTarefaUsuarioModels, RelTarefaUsuario>();

            cfg.CreateMap<RelUsuarioHabilidade, RelUsuarioHabilidadeModels>();
            cfg.CreateMap<RelUsuarioHabilidadeModels, RelUsuarioHabilidade>();

            cfg.CreateMap<Projeto, ProjetoModels>();
            cfg.CreateMap<ProjetoModels, Projeto>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors(option =>
            {
                option.AllowAnyOrigin();
                option.AllowAnyMethod();
                option.AllowAnyHeader();
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();

            app.UseSwaggerUI(opt =>
            {
                opt.SwaggerEndpoint("/swagger/v1/swagger.json", "SquadFy API");
            });
        }
    }
}
